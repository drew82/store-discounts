<?php

require_once __DIR__ . '/vendor/autoload.php';

use drew\store\Checkout;
use drew\store\MoreThanRule;
use drew\store\NthItemRule;
use drew\store\ProductFactory;

$rules = [
    new NthItemRule(ProductFactory::FRUIT_TEA, 2),
    new MoreThanRule(ProductFactory::STRAWBERRIES, 3, 4.50),
];

$basket1 = (new Checkout($rules));
$basket1->scan(ProductFactory::FRUIT_TEA);
$basket1->scan(ProductFactory::STRAWBERRIES);
$basket1->scan(ProductFactory::FRUIT_TEA);
$basket1->scan(ProductFactory::FRUIT_TEA);
$basket1->scan(ProductFactory::COFFEE);
var_dump($basket1->total === 22.45);

$basket2 = (new Checkout($rules));
$basket2->scan(ProductFactory::FRUIT_TEA);
$basket2->scan(ProductFactory::FRUIT_TEA);
var_dump($basket2->total === 3.11);

$basket3 = (new Checkout($rules));
$basket3->scan(ProductFactory::STRAWBERRIES);
$basket3->scan(ProductFactory::STRAWBERRIES);
$basket3->scan(ProductFactory::FRUIT_TEA);
$basket3->scan(ProductFactory::STRAWBERRIES);
var_dump($basket3->total === 16.61);
