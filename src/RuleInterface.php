<?php

declare(strict_types=1);

namespace drew\store;

interface RuleInterface
{
    public function apply(Basket $basket): void;
}
