<?php

declare(strict_types=1);

namespace drew\store;

/**
 * Устанавливает каждому $nth-ому товару с кодом $code
 * цену $price (по-умолчанию: 0)
 */
class NthItemRule implements RuleInterface
{
    protected string $code;
    protected int $nth;
    protected float $price;

    public function __construct(string $code, int $nth, float $price = 0)
    {
        $this->code = $code;
        $this->nth = $nth;
        $this->price = $price;
    }

    public function apply(Basket $basket): void
    {
        $products = $basket->getProductsByCode($this->code);

        $i = 0;

        foreach ($products as $idx => $product) {
            $i++;
            if ($i % $this->nth === 0) {
                $basket->setProductPrice($idx, $this->price);
            }
        }
    }
}
