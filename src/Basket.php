<?php

declare(strict_types=1);

namespace drew\store;

class Basket
{
    /**
     * @var Product[]
     */
    protected array $products = [];

    public function add(Product $product): void
    {
        $this->products[] = $product;
    }

    public function getProductsByCode(string $code): array
    {
        return array_filter($this->products, fn(Product $p) => $p->getCode() === $code);
    }

    /**
     * Заменяет имеющийся в корзине продукт на тот же, но с новой ценой
     */
    public function setProductPrice(int $idx, float $price): void
    {
        $this->products[$idx] = ProductFactory::clone($this->products[$idx], $price);
    }

    public function getTotal(): float
    {
        return array_reduce(
            $this->products,
            fn(float $carry, Product $item) => $carry + $item->getPrice(),
            0
        );
    }

    public function __toString()
    {
        return implode(PHP_EOL, $this->products);
    }
}
