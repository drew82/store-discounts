<?php

declare(strict_types=1);

namespace drew\store;

class Product
{
    protected string $code;
    protected float $price;
    protected string $description;

    public function __construct(string $code, float $price, string $description)
    {
        $this->code = $code;
        $this->price = $price;
        $this->description = $description;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function __toString()
    {
        return sprintf('%2s: %5.2f (%s)', $this->code, $this->price, $this->description);
    }
}
