<?php

declare(strict_types=1);

namespace drew\store;

use InvalidArgumentException;

class ProductFactory
{
    public const FRUIT_TEA = 'FR1';
    public const STRAWBERRIES = 'SR1';
    public const COFFEE = 'CF1';

    public static function create(string $code): Product
    {
        switch ($code) {
            case 'FR1':
                return new Product($code, 3.11, 'Fruit tea');
            case 'SR1':
                return new Product($code, 5.00, 'Strawberries');
            case 'CF1':
                return new Product($code, 11.23, 'Coffee');
            default:
                throw new InvalidArgumentException($code);
        }
    }

    public static function clone(Product $product, float $newPrice): Product
    {
        return new Product(
            $product->getCode(),
            $newPrice,
            $product->getDescription(),
        );
    }
}
