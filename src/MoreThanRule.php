<?php

declare(strict_types=1);

namespace drew\store;

/**
 * Если товаров с кодом $code в корзине больше $qty,
 * устанавливает всем им цену $price
 */
class MoreThanRule implements RuleInterface
{
    protected string $code;
    protected int $qty;
    protected float $price;

    public function __construct(string $code, int $qty, float $price)
    {
        $this->code = $code;
        $this->qty = $qty;
        $this->price = $price;
    }

    public function apply(Basket $basket): void
    {
        $products = $basket->getProductsByCode($this->code);

        if (count($products) >= $this->qty) {
            foreach ($products as $idx => $product) {
                $basket->setProductPrice($idx, $this->price);
            }
        }
    }
}
