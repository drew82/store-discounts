<?php

declare(strict_types=1);

namespace drew\store;

/**
 * @property-read float $total
 */
class Checkout
{
    protected Basket $basket;

    /**
     * @var RuleInterface[]
     */
    protected array $rules = [];

    public function __construct(array $rules)
    {
        $this->basket = new Basket();
        $this->rules = $rules;
    }

    public function scan(string $code)
    {
        $this->basket->add(ProductFactory::create($code));
    }

    public function __get($name)
    {
        $method = 'get' . ucfirst($name);

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        throw new \UnexpectedValueException($name);
    }

    protected function getTotal(): float
    {
//         echo 'before' . PHP_EOL . $this->basket, PHP_EOL;
        foreach ($this->rules as $rule) {
            $rule->apply($this->basket);
        }
//         echo 'after' . PHP_EOL . $this->basket, PHP_EOL;

        return $this->basket->getTotal();
    }
}
